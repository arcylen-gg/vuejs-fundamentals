new Vue({
    el: '#vue-app', // Root element to controlled by this vue instance
    data: { // Data Property - that are object
        job: 'Dev',
        website: 'http://arcylen.com',
        websiteTag: '<a href="http://arcylen.com">Portfolio</a>',
        name: 'Arcy',
        age: 20,
        x: 0,
        y: 0,
        a: 0,
        b: 0,
        available: true,
        nearby: false,
        error: false,
        success: false,
        pets: ['Aigoo', 'Cornietta', 'Chukoy', 'Hamtaro', 'Babats'],
        cats: [
            { name: 'Aigoo', age: 2 },
            { name: 'Cornietta', age: 9 },
            { name: 'Babats', age: 0.5 } 
        ],
        dogs: [
            { name: 'Hamtaro', age: 9 },
            { name: 'Chukoy', age: 6 }
        ],
    },
    computed: {
        compClasses: function() {
            return {
                available: this.available,
                nearby: this.nearby
            }
        },
        addToA: function() {
            console.log('AddToA');
            return this.a + this.age;
        },
        addToB: function() {
            console.log('AddToB');
            return this.b + this.age;
        },
    },
    methods: {
        // addToA: function() {
        //     console.log('AddToA');
        //     return this.a + this.age;
        // },
        // addToB: function() {
        //     console.log('AddToB');
        //     return this.b + this.age;
        // },
        logName: function() {
            console.log(`You entered your name`);
        },
        logAge: function() {
            console.log(`You entered your age`);
        },
        greet: function(time) {
            // `this` keyword refers to the instance that can access the `data`
            // property.
            // how about the `methods` property ? 
            // Yes you can access it as well 
            return `Good ${time}, ${this.sayHi()}. So you're a ${this.job}.`;
        },
        sayHi: function() {
            return `Hi ${this.name}`;
        },
        add: function(inc) {
            this.age += inc;
        },
        subtract: function(dec) {
            this.age -= dec;
        },
        updateXY: function(event) { // event will automatically get upon binding the event to a method
            this.x = event.offsetX;
            this.y = event.offsetY;
        },
        click: function() {
            alert(123);
        }
    }
});