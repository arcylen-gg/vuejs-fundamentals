var data = {
    name: 'Arcy'
};

Vue.component('greeting', {
    template: `<p>Hey there {{ name }}
    <button v-on:click="changeName"> Change name </button>
    </p>`,
    data: function() {
        return {
            name: 'Arcy'
        }
        // return data;
    },
    methods: {
        changeName: function() {
            this.name = 'Bembem';
        }
    }
});

let one = new Vue({
    el: '#vue-app-one',
    data: {
    },
    methods: {
    },
    computed: {
    }
});
let two = new Vue({
    el: '#vue-app-two',
    data: {
    },
    methods: {
    },
    computed: { 
    }
});
