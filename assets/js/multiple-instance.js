let one = new Vue({
    el: '#vue-app-one',
    data: {
        title: 'Vue App One',
        data: {
            test: 'doteng'
        }
    },
    methods: {

    },
    computed: {
        greet: function() {
            return `Hello from app one :)`;
        }
    }
});
let two = new Vue({
    el: '#vue-app-two',
    data: {
        title: 'Vue App Two'
    },
    methods: {
        changeTitle: function() {
            one.title = `Changed title from App two`;
            console.log(one.greet);
        }
    },
    computed: {
        greet: function() {
            return `Hi this is from app two`;
        }        
    }
});

two.title = `Changed from outside`;
const three = new Vue({});
console.log(three.$mount);