new Vue({
    el: "#punchbag-game",
    data: {
        health: 100, // Percent
        ended: false
    },
    methods: {
        punch: function () {
            this.health-= 10;
            this.ended = this.health <= 0 ? true : false;
        },
        restart: function () {
            this.health = 100;
            this.ended = false;
        }
    },
    computed: {

    }
});