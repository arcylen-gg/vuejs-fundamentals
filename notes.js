// What is Vue.JS
// -- A front-end Framework
// -- Create javascript drive web applications
// -- Runs in the browser
// -- No need to make multiple server request for pages
// WHY
// -- Very lean (16kb)
// -- Very high run-time performance

// 2 ways to integrate Vue JS
// CDN
// Vue-CLI & Webpack

// Understand the basic
// -- Conditionals
// -- Events
// -- Data
// 2nd stage
// -- Components
// Vue Files & templates

// LESSON 2 - The vue instance

// new Vue({}) instance do well its role is to control either the whole part
// or just a certain part of our application
// We could have an app with just one instance to controll the whole app
// We could have an app with 3 instances to control three seperate areas on that
// website [that what we call component base? YES]

// Data property and variable object to the DOM

// LESSON 3 - Data & Methods
// `this` keyword refers to the instance that can access the `data`
// property.
// how about the `methods` property ? 
// Yes you can access it as well 

// When you declare a data or method outside the root element
// you cannot access it because it only controlled the declared root element
// from `el` property (that can be class or ID)

// LESSON 04 - Data Binding
// <a v-bind:href="website" >Portfolio</a>
// <a :href="website" >Portfolio</a>
// `v-bind` or `:` on the attribute to bind the data or method to the html tag
// `v-html` to bind an HTML attributes or tag

// LESSON 05 - Events
// `v-on` - directive
// `v-on:click`
// we can use @click alternative to the `v-on` directive

// LESSON 06 - Event Modifiers
// v-on:click.once, will only fire once but if twice or thrice, no effect
// v-on:click.prevent, will prevent the default behaviour
//  && the submit event will no longer reload the page
// other modifiers are:
// -- .stop -  the click event's propagation will be stopped 
// -- .capture - use capture mode when adding the event listener 
// i.e. an event targeting an inner element is handled here before 
// being handled by that element

// -- .self - only trigger handler if event.target is the element itself
// -- .passive - the scroll event's default behavior (scrolling) will happen
// immediately, instead of waiting for `onScroll` to complete
// in case it contains `event.preventDefault()`

// modifiers can be chained for example:
// -- .stop.prevent

// LESSON 07 - Keyboard Events
// keydown, keypress, keyup
// Does `key modifier` only works with v-on directive?
// other key modifiers
// https://vuejs.org/v2/guide/events.html#Key-Modifiers
// -- .enter
// -- .tab
// -- .delete (captures both “Delete” and “Backspace” keys)
// -- .esc
// -- .space
// -- .up
// -- .down
// -- .left
// -- .right

// LESSON 08 - Two-Way Data Binding
// Using `v-model` attached the value on the input/HTML tag to the
// data property, variables

// LESSON 09 - Computed Properties
// Everytime the value changes, VueJs is going to look at these function
// on the DOM, gonna run them both even though we just change the value of `a`
// To make this more efficient we can use `Computed properties` which
// is similar to methods
// but this is gonna update when either of the value was changes

// LESSON 10 - Dynamic CSS Classes
// bind the class and pass the object properties/classes
// `key` - which is the class
// `values` - boolean

// LESSON 11 - Conditionals
// `v-if` and `v-show` directives
// `v-if` it toggles out completely, as well you can use `v-else-if`
// `v-show` doesn't take the element out but just putting a styles to hide 
// the element

// LESSON 12 - Looping with v-for
// `v-for` directive to cycle through on to array
// Show/Output the index `(cat, index) in cats`

// LESSON 13 - Punching bag game
// new thing from this tutorial is the `v-bind:style` directive
// where you can directly declared the styles with value from our data
// objects

// LESSON 14 Multiple Instance
// Instance in VUEJs is an object/class that you can access by assigning
// it of the variable then you can change/use the property and method
// from other instance
// MPJ thingking LOL ;)

// LESSON 15 Components
// Reusable piece of code or template that we can use in different instances
// data on components is not object directive
// must be a functions that returns an object
// When we're creating a vue instance likely if there's just one instances
// and that returns a single data
// However when we're creating a component there's going to be a multiple
// instance using the same kind of definition and if we just use a plane
// object instead of this function, what basically what we're doing is 
// saying all those instances are going to share that object by reference
// so they're all going to have the same object 
// and if we change the data in one of those components, it's going to update
// in every single component
// Every time we create a new instance of it we're are calling this
// function to return a fresh copy of data

// LESSON 16 - refs
// `this.$ref` other way to get the value of an attribute from HTML tag
// use ref to reference our elements on a page and grab information 
// about them